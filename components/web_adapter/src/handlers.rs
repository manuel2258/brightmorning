use std::convert::Infallible;

use log::*;

use serde::{Deserialize, Serialize};
use warp::http::StatusCode;
use warp::reply::{Json, WithStatus};
use xactor::Addr;

use bm_alarm::msg as alarm_msg;
use bm_config::msg as config_msg;
use bm_render::msg as render_msg;

use crate::msg;
use crate::server::Server;

#[derive(Serialize)]
enum ErrorOrigin {
    AlarmModule,
    RenderModule,
    ConfigModule,
    Unknown,
}

#[derive(Serialize)]
struct ErrorMessage {
    code: u16,
    reason: String,
    origin: ErrorOrigin,
}

pub async fn get_wakes(server: Addr<Server>) -> Result<impl warp::Reply, Infallible> {
    Ok(warp::reply::json(
        &server.call(msg::GetWakes {}).await.unwrap(),
    ))
}

pub async fn add_wake(
    server: Addr<Server>,
    wake: bm_config::Wake,
) -> Result<impl warp::Reply, Infallible> {
    handle_bridge_response(
        server
            .call(msg::AddWake(config_msg::AddWake { wake }))
            .await
            .unwrap(),
    )
}

#[derive(Serialize, Deserialize)]
pub struct DeleteWake {
    cron_expression: String,
}

pub async fn delete_wake(
    server: Addr<Server>,
    delete_wake: DeleteWake,
) -> Result<impl warp::Reply, Infallible> {
    handle_bridge_response(
        server
            .call(msg::DeleteWake(config_msg::DeleteWake {
                cron_expression: delete_wake.cron_expression,
            }))
            .await
            .unwrap(),
    )
}

pub async fn get_display(server: Addr<Server>) -> Result<impl warp::Reply, Infallible> {
    Ok(warp::reply::json(
        &server.call(msg::GetDisplay {}).await.unwrap(),
    ))
}

fn handle_bridge_response<T>(response: msg::Result<T>) -> Result<impl warp::Reply, Infallible> {
    match response {
        Ok(_) => Ok(warp::reply::with_status(
            warp::reply::json(&""),
            StatusCode::OK,
        )),
        Err(err) => {
            warn!("Received request that produced error: {}", err);
            match err {
                msg::Error::AlarmError(alarm_msg::Error::WakeAlreadyExisting(_)) => {
                    Ok(get_bad_error_response(err, ErrorOrigin::AlarmModule))
                }
                msg::Error::AlarmError(alarm_msg::Error::WakeNotExisting(_)) => {
                    Ok(get_bad_error_response(err, ErrorOrigin::AlarmModule))
                }
                msg::Error::AlarmError(alarm_msg::Error::CronExpressionInvalid(_)) => {
                    Ok(get_bad_error_response(err, ErrorOrigin::AlarmModule))
                }
                msg::Error::AlarmError(alarm_msg::Error::DeltaRangeInvalid(_, _)) => {
                    Ok(get_bad_error_response(err, ErrorOrigin::AlarmModule))
                }

                msg::Error::RenderError(render_msg::Error::AnimationNotFound(_)) => {
                    Ok(get_bad_error_response(err, ErrorOrigin::RenderModule))
                }
                msg::Error::RenderError(render_msg::Error::AnimationAlreadyActive(_)) => {
                    Ok(get_bad_error_response(err, ErrorOrigin::RenderModule))
                }
                msg::Error::RenderError(render_msg::Error::AnimationError(_)) => {
                    Ok(get_bad_error_response(err, ErrorOrigin::RenderModule))
                }

                msg::Error::ConfigError(config_msg::Error::WakeAlreadyRegistered(_)) => {
                    Ok(get_bad_error_response(err, ErrorOrigin::ConfigModule))
                }
                msg::Error::ConfigError(config_msg::Error::WakeNotFound(_)) => {
                    Ok(get_bad_error_response(err, ErrorOrigin::ConfigModule))
                }

                _ => Ok(get_error_response(
                    StatusCode::INTERNAL_SERVER_ERROR,
                    err,
                    ErrorOrigin::Unknown,
                )),
            }
        }
    }
}

fn get_bad_error_response(error: msg::Error, origin: ErrorOrigin) -> WithStatus<Json> {
    get_error_response(StatusCode::BAD_REQUEST, error, origin)
}

fn get_error_response(
    code: StatusCode,
    error: msg::Error,
    origin: ErrorOrigin,
) -> WithStatus<Json> {
    warp::reply::with_status(
        warp::reply::json(&ErrorMessage {
            code: code.as_u16(),
            reason: format!("{}", error),
            origin,
        }),
        code,
    )
}
