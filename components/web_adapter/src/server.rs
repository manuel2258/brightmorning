use xactor::{Actor, Caller, Context, Handler};

use log::*;

use crate::filters;
use crate::msg;

pub struct Callers {
    pub get_wakes: Caller<msg::GetWakes>,
    pub add_wake: Caller<msg::AddWake>,
    pub delete_wake: Caller<msg::DeleteWake>,
    pub get_display: Caller<msg::GetDisplay>,
}

pub struct Server {
    callers: Callers,
}

impl Server {
    pub fn new(callers: Callers) -> Self {
        Self { callers }
    }
}

#[async_trait::async_trait]
impl Actor for Server {
    async fn started(&mut self, ctx: &mut Context<Self>) -> xactor::Result<()> {
        let addr = ctx.address();
        tokio::spawn(warp::serve(filters::all(addr)).run(([127, 0, 0, 1], 2258)));

        info!("Intialized!");
        Ok(())
    }
}

#[async_trait::async_trait]
impl Handler<msg::GetWakes> for Server {
    async fn handle(&mut self, _: &mut Context<Self>, msg: msg::GetWakes) -> Vec<bm_config::Wake> {
        self.callers.get_wakes.call(msg).await.unwrap()
    }
}

#[async_trait::async_trait]
impl Handler<msg::AddWake> for Server {
    async fn handle(&mut self, _: &mut Context<Self>, msg: msg::AddWake) -> msg::Result<()> {
        self.callers.add_wake.call(msg).await.unwrap()
    }
}

#[async_trait::async_trait]
impl Handler<msg::DeleteWake> for Server {
    async fn handle(&mut self, _: &mut Context<Self>, msg: msg::DeleteWake) -> msg::Result<()> {
        self.callers.delete_wake.call(msg).await.unwrap()
    }
}

#[async_trait::async_trait]
impl Handler<msg::GetDisplay> for Server {
    async fn handle(
        &mut self,
        _: &mut Context<Self>,
        msg: msg::GetDisplay,
    ) -> Option<bm_config::Display> {
        self.callers.get_display.call(msg).await.unwrap()
    }
}
