use warp::Filter;

use crate::handlers;
use crate::server::Server;
use xactor::Addr;

const MAX_CONTENT_LENGTH: u64 = 1024 * 16;

pub fn all(
    server: Addr<Server>,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    get_wakes(server.clone())
        .or(add_wake(server.clone()))
        .or(delete_wake(server.clone()))
        .or(get_display(server.clone()))
}

pub fn get_wakes(
    server: Addr<Server>,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("wakes")
        .and(warp::get())
        .and(warp::any().map(move || server.clone()))
        .and_then(handlers::get_wakes)
}

fn add_wake(
    server: Addr<Server>,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("wakes" / "add")
        .and(warp::post())
        .and(warp::any().map(move || server.clone()))
        .and(warp::body::content_length_limit(MAX_CONTENT_LENGTH).and(warp::body::json()))
        .and_then(handlers::add_wake)
}

fn delete_wake(
    server: Addr<Server>,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("wakes" / "delete")
        .and(warp::post())
        .and(warp::any().map(move || server.clone()))
        .and(warp::body::content_length_limit(MAX_CONTENT_LENGTH).and(warp::body::json()))
        .and_then(handlers::delete_wake)
}

fn get_display(
    server: Addr<Server>,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("display")
        .and(warp::get())
        .and(warp::any().map(move || server.clone()))
        .and_then(handlers::get_display)
}
