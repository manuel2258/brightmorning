use thiserror::Error;
use xactor::message;

#[derive(Error, Debug)]
pub enum Error {
    #[error("{0}")]
    AlarmError(bm_alarm::msg::Error),
    #[error("{0}")]
    RenderError(bm_render::msg::Error),
    #[error("{0}")]
    ConfigError(bm_config::msg::Error),
}

pub type Result<T> = std::result::Result<T, Error>;

#[message(result = "Vec<bm_config::Wake>")]
pub struct GetWakes();

#[message(result = "Result<()>")]
pub struct AddWake(pub bm_config::msg::AddWake);

#[message(result = "Result<()>")]
pub struct DeleteWake(pub bm_config::msg::DeleteWake);

#[message(result = "Option<bm_config::Display>")]
pub struct GetDisplay();
