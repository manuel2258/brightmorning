use std::collections::HashMap;

use tiny_skia::Pixmap;
use xactor::{Actor, Context, Handler, Sender};

use log::*;

use crate::animation;
use crate::animation::{Animation, SimpleFade};

use crate::msg::{ActivateAnimation, DeactivateAnimation, Error, Render};

use bm_display_adapter::msg::{self as display_msg};

pub struct Renderer {
    active_animations: HashMap<String, Box<dyn Animation>>,
    animation_factories:
        HashMap<&'static str, fn(i64, i64) -> Result<Box<dyn Animation>, animation::Error>>,
    display: Sender<display_msg::Display>,
}

impl Renderer {
    pub fn new(display: Sender<display_msg::Display>) -> Self {
        let mut renderer = Self {
            active_animations: HashMap::new(),
            animation_factories: HashMap::new(),
            display,
        };
        renderer
            .animation_factories
            .insert("simple_fade", SimpleFade::new);
        debug!("Initialized!");

        renderer
    }
}

impl Actor for Renderer {}

#[async_trait::async_trait]
impl Handler<ActivateAnimation> for Renderer {
    async fn handle(&mut self, _: &mut Context<Self>, msg: ActivateAnimation) -> Result<(), Error> {
        let factory = match self.animation_factories.get(&msg.name as &str) {
            Some(func) => func,
            None => return Err(Error::AnimationNotFound(msg.name)),
        };
        let animation = match factory(msg.start_delta, msg.end_delta) {
            Ok(anim) => anim,
            Err(anim_error) => return Err(Error::AnimationError(anim_error)),
        };
        if let Some(_) = self.active_animations.get(&msg.cron_expression) {
            return Err(Error::AnimationAlreadyActive(msg.cron_expression));
        }
        assert!(self
            .active_animations
            .insert(msg.cron_expression, animation)
            .is_none());
        info!("Activeted animation {}", msg.name);
        Ok(())
    }
}

#[async_trait::async_trait]
impl Handler<DeactivateAnimation> for Renderer {
    async fn handle(
        &mut self,
        _: &mut Context<Self>,
        msg: DeactivateAnimation,
    ) -> Result<(), Error> {
        match self.active_animations.remove(&msg.cron_expression) {
            Some(_) => (),
            None => return Err(Error::AnimationNotActive(msg.cron_expression)),
        };

        info!("Deactiveted animation {}", msg.cron_expression);
        Ok(())
    }
}

#[async_trait::async_trait]
impl Handler<Render> for Renderer {
    async fn handle(&mut self, _: &mut Context<Self>, msg: Render) -> Result<(), Error> {
        if let Some(animation) = self.active_animations.get_mut(&msg.name) {
            match animation.render(Pixmap::new(32, 8).unwrap(), msg.delta) {
                Ok(pixmap) => {
                    debug!("Rendered image!");
                    let pixels = pixmap.pixels();
                    let mut real_data = Vec::new();
                    real_data.reserve(pixels.len() * 3);
                    for pixel in pixels {
                        real_data.push(pixel.red());
                        real_data.push(pixel.green());
                        real_data.push(pixel.blue());
                    }
                    self.display
                        .send(display_msg::Display { data: real_data })
                        .unwrap();
                }
                Err(err) => return Err(Error::AnimationError(err)),
            }
        } else {
            return Err(Error::AnimationNotFound(msg.name));
        }
        Ok(())
    }
}
