use crate::animation;
use thiserror::Error;
use xactor::message;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Could not find animation {0}")]
    AnimationNotFound(String),
    #[error("Animation {0} is already active")]
    AnimationAlreadyActive(String),
    #[error("Animation {0} is not active")]
    AnimationNotActive(String),
    #[error("Could not render animation: {0}")]
    AnimationError(animation::Error),
}

#[message(result = "Result<(), Error>")]
pub struct ActivateAnimation {
    pub cron_expression: String,
    pub name: String,
    pub start_delta: i64,
    pub end_delta: i64,
}

#[message(result = "Result<(), Error>")]
pub struct DeactivateAnimation {
    pub cron_expression: String,
}

#[message(result = "Result<(), Error>")]
pub struct Render {
    pub name: String,
    pub delta: i64,
}
