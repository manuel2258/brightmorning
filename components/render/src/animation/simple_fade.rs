use tiny_skia::{Paint, Pixmap, Rect, Transform};

use crate::animation::{is_in_range, Animation, Error};

pub struct SimpleFade {
    start_delta: i64,
    full_rect: Rect,
}

impl SimpleFade {
    pub fn fade(&self, pixmap: Pixmap, delta: i64) -> Pixmap {
        assert!(delta < 0);
        let relative_delta = (255. * (1. - self.start_delta as f32 / delta as f32)) as u8;
        let mut paint = Paint::default();
        paint.set_color_rgba8(255, 204, 51, relative_delta);
        self.fill(pixmap, paint)
    }

    pub fn blink(&self, pixmap: Pixmap, delta: i64) -> Pixmap {
        assert!(delta >= 0);
        let mut paint = Paint::default();
        if delta % 2 == 0 {
            paint.set_color_rgba8(255, 204, 51, 200);
        } else {
            paint.set_color_rgba8(255, 204, 51, 0);
        }
        self.fill(pixmap, paint)
    }

    pub fn fill(&self, mut pixmap: Pixmap, paint: Paint) -> Pixmap {
        pixmap
            .fill_rect(self.full_rect.clone(), &paint, Transform::default(), None)
            .unwrap();
        pixmap
    }
}

impl SimpleFade {
    pub fn new(start_delta: i64, end_delta: i64) -> Result<Box<dyn Animation>, Error> {
        is_in_range(start_delta, end_delta, -300, 60)?;
        Ok(Box::new(Self {
            start_delta,
            full_rect: Rect::from_ltrb(0., 0., 32., 8.).unwrap(),
        }))
    }
}

impl Animation for SimpleFade {
    fn render(&mut self, pixmap: Pixmap, delta: i64) -> Result<Pixmap, Error> {
        let pixmap = if delta < 0 {
            self.fade(pixmap, delta)
        } else {
            self.blink(pixmap, delta)
        };

        Ok(pixmap)
    }
}
