pub mod simple_fade;

use thiserror::Error;
use tiny_skia::Pixmap;

pub use simple_fade::SimpleFade;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Selected invalid range, {start_is} > {start_should} || {end_is} < {end_should}")]
    InvalidDeltaRange {
        start_is: i64,
        start_should: i64,
        end_is: i64,
        end_should: i64,
    },
}

pub trait Animation: Send {
    fn render(&mut self, pixmap: Pixmap, delta: i64) -> Result<Pixmap, Error>;
}

pub fn is_in_range(
    min_start_val: i64,
    max_end_val: i64,
    recv_start_delta: i64,
    recv_end_delta: i64,
) -> Result<(), Error> {
    if recv_start_delta > min_start_val || recv_end_delta < max_end_val {
        Err(Error::InvalidDeltaRange {
            start_is: recv_start_delta,
            start_should: min_start_val,
            end_is: recv_end_delta,
            end_should: max_end_val,
        })
    } else {
        Ok(())
    }
}
