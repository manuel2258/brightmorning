use thiserror::Error;
use xactor::{message, Sender};

#[derive(Debug, PartialEq, Eq, Error)]
pub enum Error {
    #[error("Could not parse cron expression \"{0}\"")]
    CronExpressionInvalid(String),
    #[error("The given delta range ({0}|{1}) is invalid")]
    DeltaRangeInvalid(i64, i64),
    #[error("A wake with the cron expression \"{0}\" is already registed")]
    WakeAlreadyExisting(String),
    #[error("A wake with the cron expression \"{0}\" does not exist")]
    WakeNotExisting(String),
}

#[message(result = "Result<(), Error>")]
pub struct AddWake {
    pub cron_expression: String,
    pub start_delta: i64,
    pub end_delta: i64,
    pub subscriber: Sender<Wake>,
}

#[message(result = "Result<(), Error>")]
pub struct DeleteWake {
    pub cron_expression: String,
    pub subscriber: Sender<Wake>,
}

#[message()]
pub struct NewTime {
    pub timestamp: i64,
}

#[message()]
#[derive(PartialEq, Eq, Debug)]
pub struct Wake {
    pub cron_expression: String,
    pub delta: i64,
}
