#[cfg(feature = "impl")]
pub mod alarm;

#[cfg(feature = "impl")]
pub use alarm::*;

pub mod msg;
