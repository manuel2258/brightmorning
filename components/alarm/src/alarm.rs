use std::{collections::HashMap, str::FromStr, time::Duration};

use chrono::{offset::Utc, DateTime, NaiveDateTime};
use cron::Schedule;
use log::*;
use xactor::{sleep, spawn, Actor, Addr, Context, Handler, Sender};

use crate::msg::*;

pub struct WakeEntry {
    schedule: Schedule,
    start_delta: i64,
    end_delta: i64,
    subscribers: Vec<Sender<Wake>>,
}

impl WakeEntry {
    pub fn add_subscriber(&mut self, msg: AddWake) -> Result<(), Error> {
        if self.subscribers.contains(&msg.subscriber) {
            Err(Error::WakeAlreadyExisting(msg.cron_expression))
        } else {
            self.subscribers.push(msg.subscriber);
            Ok(())
        }
    }

    pub fn delete_subscriber(&mut self, msg: DeleteWake) -> Result<(), Error> {
        if let Some(index) = self.subscribers.iter().position(|s| s == &msg.subscriber) {
            self.subscribers.remove(index);
            Ok(())
        } else {
            Err(Error::WakeNotExisting(msg.cron_expression))
        }
    }

    pub fn has_subscribers(&self) -> bool {
        self.subscribers.len() > 0
    }
}

impl WakeEntry {
    pub fn new(
        cron_expression: &str,
        start_delta: i64,
        end_delta: i64,
        initial_subscriber: Sender<Wake>,
    ) -> Result<Self, Error> {
        if end_delta < start_delta {
            return Err(Error::DeltaRangeInvalid(start_delta, end_delta));
        }
        if let Ok(schedule) = Schedule::from_str(cron_expression) {
            Ok(Self {
                schedule,
                start_delta,
                end_delta,
                subscribers: vec![initial_subscriber],
            })
        } else {
            Err(Error::CronExpressionInvalid(cron_expression.to_owned()))
        }
    }
}

pub struct Alarm {
    wake_entries: HashMap<String, WakeEntry>,
}

impl Alarm {
    pub fn new() -> Self {
        Self {
            wake_entries: HashMap::new(),
        }
    }

    pub async fn start_and_serve() -> Addr<Alarm> {
        let alarm_addr = Alarm::new().start().await.unwrap();
        let alarm_timer_addr = alarm_addr.clone();

        spawn(async move {
            let mut last_time_stamp = chrono::Utc::now().timestamp();
            loop {
                let new_time = chrono::Utc::now();
                let new_time_stamp = new_time.timestamp();
                if new_time_stamp > last_time_stamp {
                    last_time_stamp = new_time_stamp;
                    alarm_timer_addr
                        .send(NewTime {
                            timestamp: last_time_stamp,
                        })
                        .unwrap();
                }
                let millis_until_new_second =
                    (1000 - (new_time.timestamp_millis() % 1000) + 5) as u64;
                sleep(Duration::from_millis(millis_until_new_second)).await;
            }
        });
        debug!("Initialized!");
        alarm_addr
    }
}

impl Actor for Alarm {}

#[async_trait::async_trait]
impl Handler<AddWake> for Alarm {
    async fn handle(&mut self, _: &mut Context<Self>, msg: AddWake) -> Result<(), Error> {
        if let Some(existing_wake) = self.wake_entries.get_mut(&msg.cron_expression) {
            return existing_wake.add_subscriber(msg);
        }
        match WakeEntry::new(
            &msg.cron_expression,
            msg.start_delta,
            msg.end_delta,
            msg.subscriber,
        ) {
            Ok(wake_entry) => {
                info!("Added new wake at {:?}", &msg.cron_expression);
                self.wake_entries.insert(msg.cron_expression, wake_entry);
                Ok(())
            }
            Err(err) => Err(err),
        }
    }
}

#[async_trait::async_trait]
impl Handler<DeleteWake> for Alarm {
    async fn handle(&mut self, _: &mut Context<Self>, msg: DeleteWake) -> Result<(), Error> {
        if let Some(existing_wake) = self.wake_entries.get_mut(&msg.cron_expression) {
            info!("Deleted wake at {:?}", &msg.cron_expression);
            let cron_expression = msg.cron_expression.clone();
            existing_wake.delete_subscriber(msg)?;
            if !existing_wake.has_subscribers() {
                self.wake_entries.remove(&cron_expression).unwrap();
            }
            Ok(())
        } else {
            Err(Error::WakeNotExisting(msg.cron_expression))
        }
    }
}

#[async_trait::async_trait]
impl Handler<NewTime> for Alarm {
    async fn handle(&mut self, _: &mut Context<Self>, msg: NewTime) {
        info!("Received NewTime at {}", msg.timestamp);
        for (expr, entry) in &self.wake_entries {
            let scheduled_time = match entry
                .schedule
                .after(&DateTime::<Utc>::from_utc(
                    NaiveDateTime::from_timestamp(msg.timestamp - 1 + entry.start_delta, 0),
                    Utc,
                ))
                .take(1)
                .next()
            {
                Some(val) => val,
                None => {
                    warn!("Expression \"{}\" does not have a next time anymore!", expr);
                    continue;
                }
            }
            .timestamp();
            if scheduled_time >= msg.timestamp + entry.start_delta
                && scheduled_time <= msg.timestamp + entry.end_delta
            {
                debug!(
                    "Next scheduled timepoint for [{}]<{}|{}> is {}, which IS right now",
                    expr, entry.start_delta, entry.end_delta, scheduled_time
                );
                for subscriber in &entry.subscribers {
                    subscriber
                        .send(Wake {
                            cron_expression: expr.clone(),
                            delta: msg.timestamp - scheduled_time,
                        })
                        .unwrap();
                }
            } else {
                debug!(
                    "Next scheduled timepoint for [{}]<{}|{}> is {}, which IS NOT right now",
                    expr, entry.start_delta, entry.end_delta, scheduled_time
                );
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use simple_logger::SimpleLogger;
    use tokio::sync::{Mutex, Notify};
    use tokio::time::timeout;
    use xactor::message;

    #[message(result = "bool")]
    struct TestReceiveWake {
        wake: Wake,
    }

    struct TestActor {
        wakes: Mutex<Vec<Wake>>,
        waiting_for: Mutex<Option<Wake>>,
        notify: Notify,
    }

    impl TestActor {
        pub fn new() -> Self {
            Self {
                wakes: Mutex::new(vec![]),
                waiting_for: Mutex::new(None),
                notify: Notify::new(),
            }
        }
    }

    impl Actor for TestActor {}

    #[async_trait::async_trait]
    impl Handler<Wake> for TestActor {
        async fn handle(&mut self, _: &mut Context<Self>, msg: Wake) {
            debug!("Received wake: {:?}", &msg);
            let notify = {
                let mut waiting_for = self.waiting_for.lock().await;
                if waiting_for.is_some() {
                    let wake = waiting_for.take().unwrap();
                    info!("Test is waiting for: {:?}", &wake);
                    wake == msg
                } else {
                    false
                }
            };
            if notify {
                warn!("Test is waiting for current wake, notifying it!");
                self.notify.notify_one();
            }
            let mut wakes = self.wakes.lock().await;
            wakes.push(msg);
        }
    }

    #[async_trait::async_trait]
    impl Handler<TestReceiveWake> for TestActor {
        async fn handle(&mut self, _: &mut Context<Self>, msg: TestReceiveWake) -> bool {
            if self.wakes.lock().await.contains(&msg.wake) {
                debug!("Test already received needed wake: {:?}", &msg.wake);
                return true;
            }
            warn!(
                "Test did not yet receive wake: {:?}, waiting 100 millisecs ...",
                &msg.wake
            );
            match timeout(Duration::from_millis(100), self.notify.notified()).await {
                Ok(_) => true,
                Err(_) => false,
            }
        }
    }

    #[tokio::test]
    async fn test_should_add_wake() {
        let alarm = Alarm::new().start().await.unwrap();
        let test_actor = TestActor::new().start().await.unwrap();

        let resp = alarm
            .call(AddWake {
                cron_expression: String::from("*  *   *   *   *   *   *"),
                start_delta: 0,
                end_delta: 0,
                subscriber: test_actor.sender(),
            })
            .await
            .unwrap();

        assert!(resp.is_ok());
    }

    #[tokio::test]
    async fn test_should_not_add_wake_with_invalid_cron_expression() {
        let alarm = Alarm::new().start().await.unwrap();
        let test_actor = TestActor::new().start().await.unwrap();

        let resp = alarm
            .call(AddWake {
                cron_expression: String::from("*  *   *   *   *   *   1"),
                start_delta: 0,
                end_delta: 0,
                subscriber: test_actor.sender(),
            })
            .await
            .unwrap();

        assert_eq!(resp.unwrap_err(), Error::CronExpressionInvalid);
    }

    #[tokio::test]
    async fn test_should_not_add_wake_with_invalid_delta_ranges() {
        let alarm = Alarm::new().start().await.unwrap();
        let test_actor = TestActor::new().start().await.unwrap();

        let resp = alarm
            .call(AddWake {
                cron_expression: String::from("*  *   *   *   *   *   *"),
                start_delta: 10,
                end_delta: -10,
                subscriber: test_actor.sender(),
            })
            .await
            .unwrap();

        assert_eq!(resp.unwrap_err(), Error::DeltaRangeInvalid);
    }

    #[tokio::test]
    async fn test_should_not_add_wake_already_added_wake() {
        let alarm = Alarm::new().start().await.unwrap();
        let test_actor = TestActor::new().start().await.unwrap();

        let resp = alarm
            .call(AddWake {
                cron_expression: String::from("*  *   *   *   *   *   *"),
                start_delta: 0,
                end_delta: 0,
                subscriber: test_actor.sender(),
            })
            .await
            .unwrap();
        assert!(resp.is_ok());

        let resp = alarm
            .call(AddWake {
                cron_expression: String::from("*  *   *   *   *   *   *"),
                start_delta: 0,
                end_delta: 0,
                subscriber: test_actor.sender(),
            })
            .await
            .unwrap();

        assert_eq!(resp.unwrap_err(), Error::WakeAlreadyExisting);
    }

    #[tokio::test]
    async fn test_should_same_add_wake_twice_from_different_subscribers() {
        let alarm = Alarm::new().start().await.unwrap();
        let test_actor1 = TestActor::new().start().await.unwrap();
        let test_actor2 = TestActor::new().start().await.unwrap();

        let resp = alarm
            .call(AddWake {
                cron_expression: String::from("*  *   *   *   *   *   *"),
                start_delta: 0,
                end_delta: 0,
                subscriber: test_actor1.sender(),
            })
            .await
            .unwrap();
        assert!(resp.is_ok());

        let resp = alarm
            .call(AddWake {
                cron_expression: String::from("*  *   *   *   *   *   *"),
                start_delta: 0,
                end_delta: 0,
                subscriber: test_actor2.sender(),
            })
            .await
            .unwrap();
        assert!(resp.is_ok());
    }

    #[tokio::test]
    async fn test_should_delete_added_wake() {
        let alarm = Alarm::new().start().await.unwrap();
        let test_actor = TestActor::new().start().await.unwrap();

        let resp = alarm
            .call(AddWake {
                cron_expression: String::from("*  *   *   *   *   *   *"),
                start_delta: 0,
                end_delta: 0,
                subscriber: test_actor.sender(),
            })
            .await
            .unwrap();
        assert!(resp.is_ok());

        let resp = alarm
            .call(DeleteWake {
                cron_expression: String::from("*  *   *   *   *   *   *"),
                subscriber: test_actor.sender(),
            })
            .await
            .unwrap();
        assert!(resp.is_ok());
    }

    #[tokio::test]
    async fn test_should_not_delete_not_added_wake() {
        let alarm = Alarm::new().start().await.unwrap();
        let test_actor = TestActor::new().start().await.unwrap();

        let resp = alarm
            .call(DeleteWake {
                cron_expression: String::from("*  *   *   *   *   *   *"),
                subscriber: test_actor.sender(),
            })
            .await
            .unwrap();
        assert_eq!(resp.unwrap_err(), Error::WakeNotExisting);
    }

    #[tokio::test]
    async fn test_should_trigger_added_wake() {
        let alarm = Alarm::new().start().await.unwrap();
        let test_actor = TestActor::new().start().await.unwrap();

        let resp = alarm
            .call(AddWake {
                cron_expression: String::from("*  *   *   *   *   *   *"),
                start_delta: 0,
                end_delta: 0,
                subscriber: test_actor.sender(),
            })
            .await
            .unwrap();
        assert!(resp.is_ok());

        alarm.call(NewTime { timestamp: 42 }).await.unwrap();

        let resp = test_actor
            .call(TestReceiveWake {
                wake: Wake {
                    cron_expression: String::from("*  *   *   *   *   *   *"),
                    delta: 0,
                },
            })
            .await
            .unwrap();
        assert!(resp);

        let resp = test_actor
            .call(TestReceiveWake {
                wake: Wake {
                    cron_expression: String::from("1  *   *   *   *   *   *"),
                    delta: 0,
                },
            })
            .await
            .unwrap();
        assert!(!resp);
    }

    #[tokio::test]
    async fn test_should_trigger_added_wake_when_time_in_delta_range() {
        SimpleLogger::new().init().unwrap();
        let alarm = Alarm::new().start().await.unwrap();
        let test_actor = TestActor::new().start().await.unwrap();

        let resp = alarm
            .call(AddWake {
                cron_expression: String::from("1  9   15   16   May   Sun   2021"),
                start_delta: -5,
                end_delta: 5,
                subscriber: test_actor.sender(),
            })
            .await
            .unwrap();
        assert!(resp.is_ok());

        alarm
            .call(NewTime {
                timestamp: 1621177741,
            })
            .await
            .unwrap();
        let resp = test_actor
            .call(TestReceiveWake {
                wake: Wake {
                    cron_expression: String::from("1  9   15   16   May   Sun   2021"),
                    delta: 0,
                },
            })
            .await
            .unwrap();
        assert!(resp);

        alarm
            .call(NewTime {
                timestamp: 1621177738,
            })
            .await
            .unwrap();
        let resp = test_actor
            .call(TestReceiveWake {
                wake: Wake {
                    cron_expression: String::from("1  9   15   16   May   Sun   2021"),
                    delta: -3,
                },
            })
            .await
            .unwrap();
        assert!(resp);

        alarm
            .call(NewTime {
                timestamp: 1621177743,
            })
            .await
            .unwrap();
        let resp = test_actor
            .call(TestReceiveWake {
                wake: Wake {
                    cron_expression: String::from("1  9   15   16   May   Sun   2021"),
                    delta: 2,
                },
            })
            .await
            .unwrap();
        assert!(resp);

        alarm
            .call(NewTime {
                timestamp: 1621177730,
            })
            .await
            .unwrap();
        let resp = test_actor
            .call(TestReceiveWake {
                wake: Wake {
                    cron_expression: String::from("1  9   15   16   May   Sun   2021"),
                    delta: -11,
                },
            })
            .await
            .unwrap();
        assert!(!resp);
    }

    #[tokio::test]
    async fn test_should_not_trigger_deleted_wake() {
        let alarm = Alarm::new().start().await.unwrap();
        let test_actor = TestActor::new().start().await.unwrap();

        let resp = alarm
            .call(AddWake {
                cron_expression: String::from("*  *   *   *   *   *   *"),
                start_delta: 0,
                end_delta: 0,
                subscriber: test_actor.sender(),
            })
            .await
            .unwrap();
        assert!(resp.is_ok());

        let resp = alarm
            .call(DeleteWake {
                cron_expression: String::from("*  *   *   *   *   *   *"),
                subscriber: test_actor.sender(),
            })
            .await
            .unwrap();
        assert!(resp.is_ok());

        alarm.call(NewTime { timestamp: 42 }).await.unwrap();

        let resp = test_actor
            .call(TestReceiveWake {
                wake: Wake {
                    cron_expression: String::from("*  *   *   *   *   *   *"),
                    delta: 0,
                },
            })
            .await
            .unwrap();
        assert!(!resp);
    }
}
