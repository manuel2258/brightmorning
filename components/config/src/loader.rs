use std::{fs::File, io::BufReader, io::ErrorKind, path::Path};

use log::*;

use ron::ser::PrettyConfig;

use crate::config::{Config, Display};
use crate::msg::{Error, Result};

pub trait Loader: Send {
    fn load(&mut self) -> Result<Config>;
    fn dump(&mut self, config: &Config) -> Result<()>;
}

pub struct FileLoader {
    path: String,
    ron_pretty_config: PrettyConfig,
}

impl FileLoader {
    pub fn new(path: String) -> Box<dyn Loader> {
        Box::new(Self {
            path,
            ron_pretty_config: PrettyConfig::new()
                .with_depth_limit(4)
                .with_indentor("\t".to_owned()),
        })
    }
}

impl Loader for FileLoader {
    fn load(&mut self) -> Result<Config> {
        let config: Config = match File::open(Path::new(&self.path)) {
            Ok(file) => {
                let reader = BufReader::new(file);
                let config: Config = match ron::de::from_reader(reader) {
                    Ok(val) => val,
                    Err(err) => return Err(Error::RonError(err)),
                };
                config
            }
            Err(err) => {
                warn!(
                    "Could not load config at {}, creating empty one!",
                    &self.path
                );
                match err.kind() {
                    ErrorKind::NotFound => Config {
                        wakes: Vec::new(),
                        display: None,
                    },
                    _ => return Err(Error::IoError(err)),
                }
            }
        };
        debug!("Loaded config file from {}", &self.path);

        Ok(config)
    }

    fn dump(&mut self, config: &Config) -> Result<()> {
        let file = match File::create(&self.path) {
            Ok(val) => val,
            Err(err) => return Err(Error::IoError(err)),
        };
        match ron::ser::to_writer_pretty(file, config, self.ron_pretty_config.clone()) {
            Ok(_) => (),
            Err(err) => return Err(Error::RonError(err)),
        };
        debug!("Saved config to {}", &self.path);

        Ok(())
    }
}
