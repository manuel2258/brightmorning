use xactor::{Actor, Context, Handler};

use log::*;

use crate::config::*;
use crate::loader::{FileLoader, Loader};
use crate::msg::*;

pub struct ConfigStore {
    loader: Box<dyn Loader>,
    config: Config,
}

impl ConfigStore {
    pub fn default() -> Result<Self> {
        ConfigStore::new(FileLoader::new("config.ron".to_owned()))
    }

    pub fn new(mut loader: Box<dyn Loader>) -> Result<Self> {
        let config = loader.load()?;

        let mut config_store = Self { loader, config };
        config_store.dump()?;
        debug!("Initialized!");

        Ok(config_store)
    }

    fn dump(&mut self) -> Result<()> {
        self.loader.dump(&self.config)
    }
}

impl ConfigStore {
    fn get_wake_index(&self, cron_expression: &str) -> Option<usize> {
        self.config
            .wakes
            .iter()
            .position(|wake| &(*wake).cron_expression == cron_expression)
    }
}

#[async_trait::async_trait]
impl Actor for ConfigStore {}

#[async_trait::async_trait]
impl Handler<GetWakes> for ConfigStore {
    async fn handle(&mut self, _: &mut Context<Self>, _: GetWakes) -> Vec<Wake> {
        self.config.wakes.clone()
    }
}

#[async_trait::async_trait]
impl Handler<AddWake> for ConfigStore {
    async fn handle(&mut self, _: &mut Context<Self>, msg: AddWake) -> Result<()> {
        match self.get_wake_index(&msg.wake.cron_expression) {
            Some(_) => return Err(Error::WakeAlreadyRegistered(msg.wake.cron_expression)),
            None => self.config.wakes.push(msg.wake),
        };

        self.dump()?;
        Ok(())
    }
}

#[async_trait::async_trait]
impl Handler<DeleteWake> for ConfigStore {
    async fn handle(&mut self, _: &mut Context<Self>, msg: DeleteWake) -> Result<()> {
        let index = match self.get_wake_index(&msg.cron_expression) {
            Some(val) => val,
            None => return Err(Error::WakeNotFound(msg.cron_expression)),
        };
        self.config.wakes.remove(index);

        self.dump()?;
        Ok(())
    }
}

#[async_trait::async_trait]
impl Handler<GetDisplay> for ConfigStore {
    async fn handle(&mut self, _: &mut Context<Self>, _: GetDisplay) -> Option<Display> {
        self.config.display.clone()
    }
}

#[async_trait::async_trait]
impl Handler<SetDisplay> for ConfigStore {
    async fn handle(&mut self, _: &mut Context<Self>, msg: SetDisplay) {
        self.config.display = msg.display;
    }
}
