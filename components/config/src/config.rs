use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Clone, PartialEq, Debug)]
pub struct Wake {
    pub cron_expression: String,
    pub animation_name: String,
    pub start_delta: i64,
    pub end_delta: i64,
}

#[derive(Deserialize, Serialize, Clone, PartialEq, Debug)]
pub struct Display {
    pub address: String,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Config {
    pub wakes: Vec<Wake>,
    pub display: Option<Display>,
}
