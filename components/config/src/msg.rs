use thiserror::Error;
use xactor::message;

use crate::config::*;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Could not find wake with CronExpression \"{0}\"")]
    WakeNotFound(String),
    #[error("Wake with the given CronExpression \"{0}\" is already added")]
    WakeAlreadyRegistered(String),
    #[error("Could not interact with the config file: {0}")]
    IoError(std::io::Error),
    #[error("Could not parse config file: {0}")]
    RonError(ron::Error),
}

pub type Result<T> = std::result::Result<T, Error>;

#[message(result = "Vec<Wake>")]
pub struct GetWakes {}

#[message(result = "Result<()>")]
pub struct AddWake {
    pub wake: Wake,
}

#[message(result = "Result<()>")]
pub struct DeleteWake {
    pub cron_expression: String,
}

#[message(result = "Option<Display>")]
pub struct GetDisplay {}

#[message]
pub struct SetDisplay {
    pub display: Option<Display>,
}
