use futures_util::stream::{SplitSink, SplitStream};
use futures_util::{SinkExt, StreamExt};
use serde::Deserialize;
use tokio::net::TcpStream;
use tokio_tungstenite::{
    connect_async, tungstenite::protocol::Message, MaybeTlsStream, WebSocketStream,
};

use log::*;
use xactor::{Actor, Addr, Context, Handler};

use crate::msg::*;

#[derive(Deserialize)]
enum DisplayMessage {
    Ok,
    Error(String),
}

type WsTx = SplitSink<WebSocketStream<MaybeTlsStream<TcpStream>>, Message>;
type WsRx = SplitStream<WebSocketStream<MaybeTlsStream<TcpStream>>>;

async fn listen_to_rx(client: Addr<Client>, mut rx: WsRx) {
    while let Some(msg_result) = rx.next().await {
        match msg_result {
            Err(err) => client.send(DisplayConnectionError { err }).unwrap(),
            Ok(msg) => match msg {
                Message::Text(text) => client.send(ReceivedDisplayMessage { msg: text }).unwrap(),
                invalid_msg => error!("Received invalid msg type from display: {:?}", invalid_msg),
            },
        };
    }
    client.send(DisplayDisconnected {}).unwrap();
}

struct Connection {
    address: String,
    tx: WsTx,
}

pub struct Client {
    connection: Option<Connection>,
}

impl Client {
    pub fn new() -> Self {
        Self { connection: None }
    }

    fn get_connection(&mut self) -> Result<&mut Connection> {
        match self.connection.as_mut() {
            Some(val) => Ok(val),
            None => Err(Error::NotConnected),
        }
    }
}

impl Actor for Client {}

#[async_trait::async_trait]
impl Handler<ConnectToDisplay> for Client {
    async fn handle(&mut self, ctx: &mut Context<Self>, msg: ConnectToDisplay) -> Result<()> {
        debug!("Trying to connect to websocket at {:?}", &msg.address);
        let (ws, _) = match connect_async(&msg.address).await {
            Ok(val) => val,
            Err(err) => return Err(Error::TungsteniteError(err)),
        };

        info!("Connected to websocket at {:?}", &msg.address);

        let (tx, rx) = ws.split();
        self.connection = Some(Connection {
            address: msg.address,
            tx,
        });

        let addr = ctx.address();
        tokio::spawn(async move {
            listen_to_rx(addr, rx).await;
        });

        Ok(())
    }
}

#[async_trait::async_trait]
impl Handler<ReceivedDisplayMessage> for Client {
    async fn handle(&mut self, _: &mut Context<Self>, msg: ReceivedDisplayMessage) {
        let display_msg: DisplayMessage = match serde_json::from_str(&msg.msg) {
            Ok(val) => val,
            Err(err) => {
                error!("Received invalid msg from display: {:?} {}", &msg.msg, err);
                return;
            }
        };
        match display_msg {
            DisplayMessage::Ok => (),
            DisplayMessage::Error(err) => error!("Received error from display: {}", err),
        }
    }
}

#[async_trait::async_trait]
impl Handler<Display> for Client {
    async fn handle(&mut self, _: &mut Context<Self>, msg: Display) {
        info!(
            "Send rendered image of length {} to display",
            msg.data.len()
        );

        let con = self.get_connection().unwrap();
        con.tx.send(Message::Binary(msg.data)).await.unwrap();
    }
}

#[async_trait::async_trait]
impl Handler<DisplayConnectionError> for Client {
    async fn handle(&mut self, _: &mut Context<Self>, msg: DisplayConnectionError) {
        error!("Received DisplayConnectionError with error {}", msg.err);
    }
}

#[async_trait::async_trait]
impl Handler<DisplayDisconnected> for Client {
    async fn handle(&mut self, ctx: &mut Context<Self>, _: DisplayDisconnected) {
        error!("Received DisplayDisconnect, trying to reconnect");
        let old_connection = self.connection.take().unwrap();
        let addr = ctx.address();
        tokio::spawn(async move {
            addr.call(ConnectToDisplay {
                address: old_connection.address,
            })
            .await
            .unwrap()
            .unwrap();
        });
    }
}
