use thiserror::Error;
use tokio_tungstenite::tungstenite;
use xactor::message;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Could not connect to websocket {0}")]
    AddressNotAvailable(String),
    #[error("{0}")]
    TungsteniteError(tungstenite::Error),
    #[error("Tried to access connection while none is existing")]
    NotConnected,
}

pub type Result<T> = std::result::Result<T, Error>;

#[message(result = "Result<()>")]
pub struct ConnectToDisplay {
    pub address: String,
}

#[message(result)]
pub struct Display {
    pub data: Vec<u8>,
}

#[message]
pub struct ReceivedDisplayMessage {
    pub msg: String,
}

#[message]
pub struct DisplayDisconnected {}

#[message]
pub struct DisplayTimeouted {}

#[message]
pub struct DisplayConnectionError {
    pub err: tungstenite::Error,
}
