mod bridge;

use xactor::Actor;

use crate::bridge::Bridge;

pub fn setup_logger() -> Result<(), fern::InitError> {
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}[{}][{}] {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                record.target(),
                record.level(),
                message
            ))
        })
        .filter(|data| data.target() != "bm_alarm::alarm")
        .level(log::LevelFilter::Debug)
        .chain(std::io::stdout())
        .chain(fern::log_file("output.log")?)
        .apply()?;
    Ok(())
}

#[tokio::main(flavor = "multi_thread", worker_threads = 12)]
async fn main() {
    setup_logger().unwrap();
    let _bridge = Bridge::new().start().await.unwrap();
    loop {}
}
