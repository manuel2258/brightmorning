use xactor::{Actor, Addr, Context, Handler};

use log::*;

use bm_alarm::msg as alarm_msg;
use bm_config::msg as config_msg;
use bm_display_adapter::msg as display_msg;
use bm_render::msg as render_msg;
use bm_web_adapter::msg as web_msg;

struct Services {
    alarm: Addr<bm_alarm::Alarm>,
    renderer: Addr<bm_render::Renderer>,
    config: Addr<bm_config::ConfigStore>,
    _web_adapter: Addr<bm_web_adapter::Server>,
    display_adapter: Addr<bm_display_adapter::Client>,
}

pub struct Bridge {
    services: Option<Services>,
}

impl Bridge {
    pub fn new() -> Self {
        Self { services: None }
    }
}

impl Bridge {
    fn services(&mut self) -> &mut Services {
        self.services.as_mut().unwrap()
    }

    fn alarm(&mut self) -> &mut Addr<bm_alarm::Alarm> {
        &mut self.services().alarm
    }

    fn renderer(&mut self) -> &mut Addr<bm_render::Renderer> {
        &mut self.services().renderer
    }

    fn config(&mut self) -> &mut Addr<bm_config::ConfigStore> {
        &mut self.services().config
    }

    fn display_adapter(&mut self) -> &mut Addr<bm_display_adapter::Client> {
        &mut self.services().display_adapter
    }

    async fn add_wake(
        &mut self,
        ctx: &mut Context<Self>,
        wake: bm_config::Wake,
        add_to_config: bool,
    ) -> web_msg::Result<()> {
        if let Err(alarm_err) = self
            .alarm()
            .call(alarm_msg::AddWake {
                cron_expression: wake.cron_expression.clone(),
                start_delta: wake.start_delta,
                end_delta: wake.end_delta,
                subscriber: ctx.address().sender(),
            })
            .await
            .unwrap()
        {
            return Err(web_msg::Error::AlarmError(alarm_err));
        };
        if let Err(render_err) = self
            .renderer()
            .call(bm_render::msg::ActivateAnimation {
                cron_expression: wake.cron_expression.clone(),
                name: wake.animation_name.clone(),
                start_delta: wake.start_delta,
                end_delta: wake.end_delta,
            })
            .await
            .unwrap()
        {
            self.alarm()
                .call(alarm_msg::DeleteWake {
                    cron_expression: wake.cron_expression,
                    subscriber: ctx.address().sender(),
                })
                .await
                .unwrap()
                .unwrap();
            return Err(web_msg::Error::RenderError(render_err));
        };
        if add_to_config {
            self.config()
                .call(config_msg::AddWake { wake })
                .await
                .unwrap()
                .unwrap();
        }
        Ok(())
    }

    async fn delete_wake(
        &mut self,
        ctx: &mut Context<Self>,
        cron_expression: String,
    ) -> web_msg::Result<()> {
        if let Err(alarm_err) = self
            .alarm()
            .call(alarm_msg::DeleteWake {
                cron_expression: cron_expression.clone(),
                subscriber: ctx.address().sender(),
            })
            .await
            .unwrap()
        {
            return Err(web_msg::Error::AlarmError(alarm_err));
        };
        if let Err(render_err) = self
            .renderer()
            .call(render_msg::DeactivateAnimation {
                cron_expression: cron_expression.clone(),
            })
            .await
            .unwrap()
        {
            return Err(web_msg::Error::RenderError(render_err));
        };
        if let Err(config_err) = self
            .config()
            .call(config_msg::DeleteWake { cron_expression })
            .await
            .unwrap()
        {
            return Err(web_msg::Error::ConfigError(config_err));
        };

        Ok(())
    }

    async fn get_display(&mut self) -> Option<bm_config::config::Display> {
        self.config().call(config_msg::GetDisplay {}).await.unwrap()
    }

    async fn set_display(
        &mut self,
        display: Option<bm_config::config::Display>,
        set_in_config: bool,
    ) {
        if display.is_some() {
            self.display_adapter()
                .call(display_msg::ConnectToDisplay {
                    address: display.clone().unwrap().address,
                })
                .await
                .unwrap()
                .unwrap();
        }
        if set_in_config {
            self.config()
                .call(config_msg::SetDisplay { display })
                .await
                .unwrap();
        }
    }
}

#[async_trait::async_trait]
impl Actor for Bridge {
    async fn started(&mut self, ctx: &mut Context<Self>) -> xactor::Result<()> {
        info!("Initializing services");
        let display_adapter = bm_display_adapter::Client::new().start().await?;
        let services = Services {
            alarm: bm_alarm::Alarm::start_and_serve().await,
            renderer: bm_render::Renderer::new(display_adapter.sender())
                .start()
                .await?,
            config: bm_config::ConfigStore::default()?.start().await?,
            _web_adapter: bm_web_adapter::Server::new(bm_web_adapter::Callers {
                get_wakes: ctx.address().caller(),
                add_wake: ctx.address().caller(),
                delete_wake: ctx.address().caller(),
                get_display: ctx.address().caller(),
            })
            .start()
            .await?,
            display_adapter,
        };
        self.services = Some(services);

        let wakes = self.config().call(bm_config::msg::GetWakes {}).await?;

        for wake in wakes {
            self.add_wake(ctx, wake, false).await?;
        }

        let display = self.get_display().await;
        self.set_display(display, false).await;

        Ok(())
    }
}

#[async_trait::async_trait]
impl Handler<alarm_msg::Wake> for Bridge {
    async fn handle(&mut self, _: &mut Context<Self>, msg: alarm_msg::Wake) {
        self.renderer()
            .call(render_msg::Render {
                name: msg.cron_expression,
                delta: msg.delta,
            })
            .await
            .unwrap()
            .unwrap();
    }
}

#[async_trait::async_trait]
impl Handler<web_msg::GetWakes> for Bridge {
    async fn handle(
        &mut self,
        _: &mut Context<Self>,
        _: web_msg::GetWakes,
    ) -> Vec<bm_config::Wake> {
        self.config().call(config_msg::GetWakes {}).await.unwrap()
    }
}

#[async_trait::async_trait]
impl Handler<web_msg::AddWake> for Bridge {
    async fn handle(
        &mut self,
        ctx: &mut Context<Self>,
        msg: web_msg::AddWake,
    ) -> web_msg::Result<()> {
        self.add_wake(ctx, msg.0.wake, true).await
    }
}

#[async_trait::async_trait]
impl Handler<web_msg::DeleteWake> for Bridge {
    async fn handle(
        &mut self,
        ctx: &mut Context<Self>,
        msg: web_msg::DeleteWake,
    ) -> web_msg::Result<()> {
        self.delete_wake(ctx, msg.0.cron_expression).await
    }
}

#[async_trait::async_trait]
impl Handler<web_msg::GetDisplay> for Bridge {
    async fn handle(
        &mut self,
        ctx: &mut Context<Self>,
        _: web_msg::GetDisplay,
    ) -> Option<bm_config::Display> {
        self.config().call(config_msg::GetDisplay {}).await.unwrap()
    }
}
